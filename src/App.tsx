import React from 'react';
import logo from './logo.svg';
import './App.css';
import TestButtons from './Components/TestButtons';

function App() {
  return (
    <div className="App">
      <TestButtons />
    </div>
  );
}

export default App;
