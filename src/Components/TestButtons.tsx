import React, { useState } from 'react';
import { Button, Paper } from '@mui/material';
import axios from 'axios';

function TestButtons() {

  const [error, setError] = useState<string | null>(null);
  const [text, setText] = useState<string | null>(null);
  const get = () => {
    axios<string>(`${process.env.REACT_APP_BACK_URL}/my/guid`)
      .then(res =>{ 
        setError(null);
        setText(res.data);
      })
      .catch(error => setError(JSON.stringify(error)));
  };

  const put = () => {
    axios.put(`${process.env.REACT_APP_BACK_URL}/my/guid`)
      .then(res =>{ 
        setError(null);
        setText(res.data);
      })
      .catch(error => setError(JSON.stringify(error)));
  };


  return (
    <div className="App">
      <Button onClick={get} variant='contained'>Клик GET</Button>
      <Button onClick={put} variant='contained'>Клик PUT</Button>
      {text && <Paper className="p-10" elevation={2}>
        {text}
      </Paper>}
      {error && <Paper className="red p-10" elevation={2} >
        {error}
      </Paper >}
    </div>


  );
}

export default TestButtons;